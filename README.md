# Введение в Spring Framework

## Домашнее задание №4

Приложеение работает с помощью Spring Shell. Пользователю доступны 3 команды:
0. start - начать тестирование. Команда доступна только после регистрации студента
0. login - регистрация в приложении. Необходимо ввести имя и фамилию
0. changeLang - позволяет изменить язык интерфейса приложения

---
## Домашнее задание №3

- Перенести приложение на Spring Boot
- Перенести все свойства в application.yml
- Сделать собственный баннер для приложения.
- Перенести тесты и использовать spring-boot-test-starter

