package ru.otus.spring.framework01;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.context.annotation.Import;
import ru.otus.spring.framework01.config.AppLocaleConfig;
import ru.otus.spring.framework01.dao.CsvFileReaderDao;
import ru.otus.spring.framework01.domain.AnswerChoice;
import ru.otus.spring.framework01.domain.Question;
import ru.otus.spring.framework01.view.AppLocale;
import ru.otus.spring.framework01.view.LocaleType;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

@DisplayName("Test of CSV file reader")
@SpringBootTest (properties = "spring.shell.interactive.enabled=false")
@Import(TestConf.class)
class CsvFileReaderDaoTest {

    @SpyBean
    private AppLocale appLocale;

    @SpyBean
    private AppLocaleConfig appLocaleConfig;

    @Autowired
    private CsvFileReaderDao csvFileReader;

    @AfterEach
    public void tearDown() {
        reset(appLocaleConfig);
    }

    @Test
    @DisplayName("Success method getAllQuestion for Eng local")
    public void successGetAllQuestionEngLocal() {
        when(appLocale.getCurrentLocale()).thenReturn(LocaleType.EN);
        List<Question> allQuestion = csvFileReader.getAllQuestion();
        assertEquals(1, allQuestion.size());
        Question actQuestion = allQuestion.get(0);
        Question expQuestion = prepareQuestion("ABCD."
                , new String[][]{{"1", "F"}, {"2", "G"}, {"5", "E"}, {"3", "H"}}
                , "5");
        assertEquals(expQuestion, actQuestion);
    }

    @Test
    @DisplayName("Success method getAllQuestion for Ru local")
    public void successGetAllQuestionRuLocal() {
        when(appLocale.getCurrentLocale()).thenReturn(LocaleType.RU);
        List<Question> allQuestion = csvFileReader.getAllQuestion();
        assertEquals(1, allQuestion.size());
        Question actQuestion = allQuestion.get(0);
        Question expQuestion = prepareQuestion("АБВГД..."
                , new String[][]{{"1", "К"}, {"2", "Ц"}, {"5", "Е"}, {"3", "Д"}}
                , "5");
        assertEquals(expQuestion, actQuestion);
    }

    @Test
    @DisplayName("The file with question is not exist")
    public void fileWithQuestionNotExist() {
        when(appLocale.getCurrentLocale()).thenReturn(LocaleType.RU);
        doReturn("/question_dao/qen.csv").when(appLocaleConfig).getFileQuestions();
        doReturn("/question_dao/qru.csv").when(appLocaleConfig).getFileQuestionsRu();
        assertThrows(NullPointerException.class, () -> csvFileReader.getAllQuestion());
    }

    public Question prepareQuestion(String qText, String[][] answersChoice, String correctAnswerCode) {
        Question question = new Question();
        question.setText(qText);

        List<AnswerChoice> list = new ArrayList<>(answersChoice.length);
        question.setAnswerChoices(list);
        for (String[] strings : answersChoice) {
            list.add(prepareAnswerChoice(strings[0], strings[1]));
        }

        question.setCorrectAnswerCode(correctAnswerCode);
        return question;
    }

    private AnswerChoice prepareAnswerChoice(String seqCode, String ansChoiceText) {
        AnswerChoice ansChoice = new AnswerChoice();
        ansChoice.setSequenceCode(seqCode);
        ansChoice.setText(ansChoiceText);

        return ansChoice;
    }
}
