package ru.otus.spring.framework01;

import org.mockito.Mockito;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import ru.otus.spring.framework01.controller.QuestionController;
import ru.otus.spring.framework01.controller.ScoreCalculator;
import ru.otus.spring.framework01.controller.TestController;
import ru.otus.spring.framework01.controller.implementations.TestControllerImpl;
import ru.otus.spring.framework01.dao.QuestionDao;
import ru.otus.spring.framework01.domain.User;
import ru.otus.spring.framework01.view.AppLocale;
import ru.otus.spring.framework01.view.MessageService;
import ru.otus.spring.framework01.view.ViewController;

import static org.mockito.ArgumentMatchers.any;

@TestConfiguration
public class TestConf {

    @Bean
    public TestController testController(
            ViewController viewController, QuestionController questionController,
            QuestionDao questionDao, ScoreCalculator scoreCalculator,
            MessageService messageService, AppLocale appLocale
    ) throws CloseApplicationException {
        TestController testController = new TestControllerImpl(viewController, questionController
                , questionDao, scoreCalculator, messageService, appLocale);
        TestController spyTestController = Mockito.spy(testController);
        Mockito.doNothing().when(spyTestController).runTest(any(User.class));

        return spyTestController;
    }
}
