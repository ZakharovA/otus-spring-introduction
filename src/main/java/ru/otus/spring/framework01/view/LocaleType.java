package ru.otus.spring.framework01.view;

import org.apache.commons.lang3.StringUtils;

import java.util.Locale;

public enum LocaleType {
    EN(Locale.ENGLISH, "en", "English"),
    RU(new Locale("ru", "RU"), "ru", "Русский");

    private final Locale locale;
    private final String title;
    private final String key;

    LocaleType(Locale locale, String key, String title) {
        this.locale = locale;
        this.title = title;
        this.key = key;
    }

    public Locale getLocale() {
        return locale;
    }

    public String getTitle() {
        return title;
    }

    public String getKey() {
        return key;
    }

    public static LocaleType getLocaleType(String key) {
        for (LocaleType value : LocaleType.values()) {
            if (StringUtils.equals(value.getKey(), key)) {
                return value;
            }
        }

        throw new IllegalArgumentException();
    }


    @Override
    public String toString() {
        return key;
    }
}
