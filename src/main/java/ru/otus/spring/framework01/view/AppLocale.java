package ru.otus.spring.framework01.view;

public interface AppLocale {

    void changeLocale(String key) throws IllegalLocaleException;

    LocaleType getCurrentLocale();
}
