package ru.otus.spring.framework01.view;

public interface MessageService {

    String getMessageValue(String code);
}
