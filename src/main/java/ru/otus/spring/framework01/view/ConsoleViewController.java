package ru.otus.spring.framework01.view;

import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import ru.otus.spring.framework01.CloseApplicationException;

import java.util.Collections;
import java.util.List;
import java.util.Scanner;

/**
 * @author aszakh
 */
@Service
@RequiredArgsConstructor
public class ConsoleViewController implements ViewController {

    private final Scanner scanner = new Scanner(System.in);

    private final MessageService messageService;

    @Override
    public void println(String message) {
        System.out.println(message);
    }

    @Override
    public String getAnswer(String question, String answerChoice, List<String> allowedAnswers) throws CloseApplicationException {
        String line;
        do {
            System.out.println(question);
            System.out.println(answerChoice);
            System.out.print(messageService.getMessageValue("input.answer.message"));
            line = scanner.nextLine();
        } while (isShortKey(line)
                || isNotAllowedAnswer(allowedAnswers, line));

        return line;
    }

    @Override
    public String getAnswer(String question) throws CloseApplicationException {
        return getAnswer(question, Collections.emptyList());
    }

    @Override
    public String getAnswer(String question, List<String> allowedAnswers) throws CloseApplicationException {
        String line;
        do {
            System.out.print(question);
            line = scanner.nextLine();
        } while (isShortKey(line) || isNotAllowedAnswer(allowedAnswers, line));

        return line;
    }

    private boolean isNotAllowedAnswer(List<String> allowedAnswers, String inputAnswer) {
        if (allowedAnswers == null || allowedAnswers.isEmpty()) {
            return false;
        }

        if (allowedAnswers.stream().noneMatch(inputAnswer::equals)) {
            println(messageService.getMessageValue("error.message.incorrect.answer.code"));
            return true;
        }

        return false;
    }

    private boolean isShortKey(String line) throws CloseApplicationException {
        if (StringUtils.equals("-q", line) || StringUtils.equals("quit", line)) {
            throw new CloseApplicationException();
        } else if (StringUtils.equals("-h", line)) {
            printHelp();
            return true;
        }

        return false;
    }

    private void printHelp() {
        System.out.println(messageService.getMessageValue("help.message"));
    }
}
