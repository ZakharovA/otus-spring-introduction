package ru.otus.spring.framework01.view;

import ru.otus.spring.framework01.CloseApplicationException;

import java.util.List;

public interface ViewController {

    void println(String message);

    String getAnswer(String question, String answerChoice, List<String> allowedAnswers) throws CloseApplicationException;

    String getAnswer(String question) throws CloseApplicationException;

    String getAnswer(String question, List<String> allowedAnswers) throws CloseApplicationException;
}
