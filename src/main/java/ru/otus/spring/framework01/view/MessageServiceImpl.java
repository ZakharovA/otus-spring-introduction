package ru.otus.spring.framework01.view;

import lombok.RequiredArgsConstructor;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class MessageServiceImpl implements MessageService, AppLocale {

    private final MessageSource messageSource;
    private LocaleType currentLocale = LocaleType.EN;

    @Override
    public void changeLocale(String key) throws IllegalLocaleException {
        try {
            currentLocale = LocaleType.getLocaleType(key);
        } catch (IllegalArgumentException ex) {
            throw new IllegalLocaleException();
        }
    }

    @Override
    public LocaleType getCurrentLocale() {
        return currentLocale;
    }

    @Override
    public String getMessageValue(String code) {
        return messageSource.getMessage(code, null, currentLocale.getLocale());
    }

}
