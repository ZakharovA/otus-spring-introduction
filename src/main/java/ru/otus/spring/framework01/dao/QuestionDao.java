package ru.otus.spring.framework01.dao;

import ru.otus.spring.framework01.domain.Question;

import java.util.List;

/**
 * @author aszakh
 */
public interface QuestionDao {

    List<Question> getAllQuestion();
}
