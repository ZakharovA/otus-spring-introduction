package ru.otus.spring.framework01.dao;

import lombok.RequiredArgsConstructor;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.springframework.stereotype.Service;
import ru.otus.spring.framework01.Application;
import ru.otus.spring.framework01.config.AppLocaleConfig;
import ru.otus.spring.framework01.domain.AnswerChoice;
import ru.otus.spring.framework01.domain.Question;
import ru.otus.spring.framework01.view.AppLocale;
import ru.otus.spring.framework01.view.LocaleType;

import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author aszakh
 */
@Service
@RequiredArgsConstructor
public class CsvFileReaderDao implements QuestionDao {

    private final AppLocaleConfig appLocaleConfig;
    private final AppLocale appLocale;

    @Override
    public List<Question> getAllQuestion() {
        try {
            return readFile();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return Collections.emptyList();
    }

    private List<Question> readFile() throws IOException {
        String pathQuestions = getPathWithQuestions();
        try (Reader in = new FileReader(Application.class.getResource(pathQuestions).getFile())) {
            List<CSVRecord> records = CSVFormat.DEFAULT
                    .withHeader(FileHeaderColumn.QUESTION.getTitle()
                            , FileHeaderColumn.ANSWER_CHOICE.getTitle(), FileHeaderColumn.RIGHT_ANSWER_CODE.getTitle())
                    .withDelimiter(',').withIgnoreEmptyLines().withSkipHeaderRecord().parse(in).getRecords();
            if (records.isEmpty()) {
                return Collections.emptyList();
            }
            List<Question> questions = new ArrayList<>(records.size());
            for (CSVRecord record : records) {
                Question q = new Question();
                q.setText(record.get(FileHeaderColumn.QUESTION.getTitle()));
                q.setAnswerChoices(fetchAnswerChoice(record.get(FileHeaderColumn.ANSWER_CHOICE.getTitle())));
                q.setCorrectAnswerCode(record.get(FileHeaderColumn.RIGHT_ANSWER_CODE.getTitle()));
                questions.add(q);
            }
            return questions;
        }
    }

    private String getPathWithQuestions() {
        if (LocaleType.RU == appLocale.getCurrentLocale()) {
            return appLocaleConfig.getFileQuestionsRu();
        }

        return appLocaleConfig.getFileQuestions();
    }

    private List<AnswerChoice> fetchAnswerChoice(String answerChoice) {
        String[] split = answerChoice.split(";");
        List<AnswerChoice> list = new ArrayList<>(split.length);
        for (String s : split) {
            String[] sEntity = s.split(":");
            if (sEntity.length == 2) {
                AnswerChoice entity = new AnswerChoice();
                entity.setSequenceCode(sEntity[0]);
                entity.setText(sEntity[1]);
                list.add(entity);
            }
        }
        return list;
    }

    enum FileHeaderColumn {
        QUESTION("Question"),
        ANSWER_CHOICE("AnswerChoice"),
        RIGHT_ANSWER_CODE("CorrectAnswerCode");

        final String title;

        FileHeaderColumn(String title) {
            this.title = title;
        }

        String getTitle() {
            return title;
        }
    }
}
