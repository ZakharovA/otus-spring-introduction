package ru.otus.spring.framework01.config;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

@NoArgsConstructor
@Getter
@Setter
@ConfigurationProperties(prefix = "app.path")
public class AppLocaleConfig {

    private String fileQuestions;
    private String fileQuestionsRu;
    private String folderLocale;

}
