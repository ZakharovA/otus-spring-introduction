package ru.otus.spring.framework01.config;

import ru.otus.spring.framework01.view.MessageService;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.ArrayList;
import java.util.List;

public class EnumValidatorImpl implements ConstraintValidator<EnumValidator, String> {

    private List<String> valueList = null;
    private MessageService messageService;

    public EnumValidatorImpl(MessageService messageService) {
        this.messageService = messageService;
    }

    @Override
    public void initialize(EnumValidator constraintAnnotation) {
        Class<? extends Enum<?>> enumClass = constraintAnnotation.enumClass();
        Enum[] enumValArr = enumClass.getEnumConstants();

        valueList = new ArrayList<>(enumValArr.length);
        for (Enum enumVal : enumValArr) {
            valueList.add(enumVal.toString());
        }
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        boolean result = valueList.stream().anyMatch(value::equals);
        if (!result) {
            context.disableDefaultConstraintViolation();
            context.buildConstraintViolationWithTemplate(messageService.getMessageValue("change.lang.illegal.key.message")).addConstraintViolation();
        }
        return result;
    }
}
