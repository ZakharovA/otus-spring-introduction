package ru.otus.spring.framework01;

import lombok.RequiredArgsConstructor;
import org.springframework.shell.Availability;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import org.springframework.shell.standard.ShellMethodAvailability;
import org.springframework.shell.standard.ShellOption;
import ru.otus.spring.framework01.config.EnumValidator;
import ru.otus.spring.framework01.controller.AuthorizationUser;
import ru.otus.spring.framework01.controller.TestController;
import ru.otus.spring.framework01.controller.UnauthorizedUserException;
import ru.otus.spring.framework01.domain.User;
import ru.otus.spring.framework01.view.LocaleType;
import ru.otus.spring.framework01.view.MessageService;
import ru.otus.spring.framework01.view.ViewController;

import javax.validation.constraints.NotBlank;


@ShellComponent
@RequiredArgsConstructor
public class ShellRunner {

    private final TestController testController;
    private final AuthorizationUser authorizationUser;
    private final ViewController viewController;
    private final MessageService messageService;

    private User user;

    @ShellMethod("Start testing")
    @ShellMethodAvailability("availabilityStart")
    public void start() {
        try {
            testController.runTest(user);
        } catch (CloseApplicationException e) {
            System.exit(1);
        }
    }

    private Availability availabilityStart() {
        return user != null ?
                Availability.available() :
                Availability.unavailable(messageService.getMessageValue("authorized.error.message"));
    }

    @ShellMethod("Enter the student's name and surname")
    public void login(
            @ShellOption(help = "User name") @NotBlank String name,
            @ShellOption(help = "User surname") @NotBlank String surname
    ) {
        try {
            user = authorizationUser.authorization(name, surname);
        } catch (UnauthorizedUserException e) {
            viewController.println(e.getMessage());
            user = null;
        }
    }

    @ShellMethod("Change language")
    public void changeLang(
            @ShellOption(help = "Language key")
            @EnumValidator(enumClass = LocaleType.class) String langKey
    ) {
        testController.changeLocale(langKey);
    }
}
