package ru.otus.spring.framework01.domain;

import lombok.Data;

/**
 * @author aszakh
 */
@Data
public class User {

    private final String name;
    private final String surname;

}
