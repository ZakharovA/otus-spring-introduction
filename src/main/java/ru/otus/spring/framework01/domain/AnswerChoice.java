package ru.otus.spring.framework01.domain;

import lombok.Data;

/**
 * @author aszakh
 */
@Data
public class AnswerChoice {

    private String sequenceCode;
    private String text;
}
