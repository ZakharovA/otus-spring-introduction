package ru.otus.spring.framework01.domain;

import lombok.Data;

import java.util.List;

/**
 * @author aszakh
 */
@Data
public class Question {

    private String text;
    private List<AnswerChoice> answerChoices;
    private String correctAnswerCode;
}
