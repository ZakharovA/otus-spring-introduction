package ru.otus.spring.framework01.controller.implementations;

import lombok.val;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.stereotype.Service;
import ru.otus.spring.framework01.controller.ScoreCalculator;
import ru.otus.spring.framework01.domain.Question;

import java.util.List;

/**
 * @author aszakh
 */
@Service
public class ScoreCalculatorImpl implements ScoreCalculator {

    @Override
    public int calculate(List<Pair<String, Question>> testResults) {

        int rightQuestions = 0;

        for (Pair<String, Question> result : testResults) {
            if (StringUtils.equals(result.getLeft(), result.getRight().getCorrectAnswerCode())) {
                rightQuestions++;
            }
        }

        val percentCorrectAnswer = (double) rightQuestions / testResults.size();

        if (percentCorrectAnswer <= 1 && percentCorrectAnswer >= 0.9) {
            return 5;
        } else if (percentCorrectAnswer >= 0.8 && percentCorrectAnswer < 0.9) {
            return 4;
        } else if (percentCorrectAnswer >= 0.5 && percentCorrectAnswer < 0.8) {
            return 3;
        }

        return 2;
    }
}
