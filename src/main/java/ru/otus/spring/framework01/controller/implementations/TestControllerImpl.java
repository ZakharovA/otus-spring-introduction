package ru.otus.spring.framework01.controller.implementations;

import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.stereotype.Service;
import ru.otus.spring.framework01.CloseApplicationException;
import ru.otus.spring.framework01.controller.QuestionController;
import ru.otus.spring.framework01.controller.ScoreCalculator;
import ru.otus.spring.framework01.controller.TestController;
import ru.otus.spring.framework01.dao.QuestionDao;
import ru.otus.spring.framework01.domain.Question;
import ru.otus.spring.framework01.domain.User;
import ru.otus.spring.framework01.view.AppLocale;
import ru.otus.spring.framework01.view.IllegalLocaleException;
import ru.otus.spring.framework01.view.MessageService;
import ru.otus.spring.framework01.view.ViewController;

import java.util.Arrays;
import java.util.List;

/**
 * @author aszakh
 */
@Service
@RequiredArgsConstructor
public class TestControllerImpl implements TestController {

    private final ViewController viewController;
    private final QuestionController questionController;
    private final QuestionDao questionDao;
    private final ScoreCalculator scoreCalculator;
    private final MessageService messageService;
    private final AppLocale appLocale;

    @SuppressWarnings("InfiniteLoopStatement")
    @Override
    public void runTest(User user) throws CloseApplicationException {
        do {
            List<Question> questions = questionDao.getAllQuestion();
            if (questions != null && !questions.isEmpty()) {
                List<Pair<String, Question>> testResults = questionController.askQuestions(questions);
                int userScore = scoreCalculator.calculate(testResults);
                viewController.println(String.format(
                        messageService.getMessageValue("score.result.message")
                        , user.getSurname(), user.getName(), userScore));
            } else {
                viewController.println("No questions");
            }
        } while (StringUtils.equalsIgnoreCase("y", viewController.getAnswer(
                messageService.getMessageValue("repeat.test.message"), Arrays.asList("y", "n"))));
    }

    @Override
    public boolean changeLocale(String langKey) {
        try {
            appLocale.changeLocale(langKey);
        } catch (IllegalLocaleException e) {
            viewController.println(messageService.getMessageValue("change.lang.illegal.key.message"));
            return false;
        }
        viewController.println(messageService.getMessageValue("change.lang.done.message"));
        return true;
    }
}
