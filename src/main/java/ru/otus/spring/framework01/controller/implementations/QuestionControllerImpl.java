package ru.otus.spring.framework01.controller.implementations;

import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.stereotype.Service;
import ru.otus.spring.framework01.CloseApplicationException;
import ru.otus.spring.framework01.controller.QuestionController;
import ru.otus.spring.framework01.domain.AnswerChoice;
import ru.otus.spring.framework01.domain.Question;
import ru.otus.spring.framework01.view.ViewController;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author aszakh
 */
@Service
@RequiredArgsConstructor
public class QuestionControllerImpl implements QuestionController {

    private final ViewController viewController;

    @Override
    public List<Pair<String, Question>> askQuestions(List<Question> questions) throws CloseApplicationException {
        List<Pair<String, Question>> testResults = new ArrayList<>(questions.size());

        for (Question q : questions) {
            String answerCode;

            String answerChoice = q.getAnswerChoices().stream()
                    .map(ansChoice -> ansChoice.getSequenceCode() + ": " + ansChoice.getText() + ";" + "\t")
                    .reduce("", String::concat);
            List<String> availableAnswerCodes = q.getAnswerChoices().stream().map(AnswerChoice::getSequenceCode).collect(Collectors.toList());
            answerCode = viewController.getAnswer("---\nQ: " + q.getText(), answerChoice, availableAnswerCodes);
            testResults.add(new ImmutablePair<>(answerCode, q));
        }

        return testResults;
    }
}
