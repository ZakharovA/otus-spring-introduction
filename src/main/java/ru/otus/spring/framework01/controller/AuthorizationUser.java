package ru.otus.spring.framework01.controller;

import ru.otus.spring.framework01.domain.User;

public interface AuthorizationUser {
    User authorization(String name, String surname) throws UnauthorizedUserException;
}
