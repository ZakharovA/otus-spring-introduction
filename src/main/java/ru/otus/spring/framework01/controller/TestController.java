package ru.otus.spring.framework01.controller;

import ru.otus.spring.framework01.CloseApplicationException;
import ru.otus.spring.framework01.domain.User;

public interface TestController {
    void runTest(User user) throws CloseApplicationException;

    boolean changeLocale(String langKey);
}
