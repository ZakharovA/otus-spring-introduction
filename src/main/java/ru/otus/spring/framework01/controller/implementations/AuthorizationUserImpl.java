package ru.otus.spring.framework01.controller.implementations;

import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import ru.otus.spring.framework01.controller.AuthorizationUser;
import ru.otus.spring.framework01.controller.UnauthorizedUserException;
import ru.otus.spring.framework01.domain.User;
import ru.otus.spring.framework01.view.MessageService;

/**
 * @author aszakh
 */
@Service
@RequiredArgsConstructor
public class AuthorizationUserImpl implements AuthorizationUser {

    private final MessageService messageService;

    @Override
    public User authorization(String name, String surname) throws UnauthorizedUserException {
        if (StringUtils.isEmpty(name)) {
            throw new UnauthorizedUserException(messageService.getMessageValue("error.message.unspecified.name"));
        }

        if (StringUtils.isEmpty(surname)) {
            throw new UnauthorizedUserException(messageService.getMessageValue("error.message.unspecified.surname"));
        }

        return new User(name, surname);
    }


}
