package ru.otus.spring.framework01.controller;

import org.apache.commons.lang3.tuple.Pair;
import ru.otus.spring.framework01.CloseApplicationException;
import ru.otus.spring.framework01.domain.Question;

import java.util.List;

public interface QuestionController {
    List<Pair<String, Question>> askQuestions(List<Question> questions) throws CloseApplicationException;
}
