package ru.otus.spring.framework01.controller;

/**
 * @author aszakh
 */
public class UnauthorizedUserException extends Exception {

    public UnauthorizedUserException(String message) {
        super(message);
    }
}
