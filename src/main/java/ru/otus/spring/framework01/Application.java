package ru.otus.spring.framework01;

import lombok.RequiredArgsConstructor;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import ru.otus.spring.framework01.config.AppLocaleConfig;

/**
 * @author aszakh
 */
@SpringBootApplication
@RequiredArgsConstructor
@EnableConfigurationProperties(AppLocaleConfig.class)
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}
